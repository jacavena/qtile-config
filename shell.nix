{ pkgs ? import <nixpkgs> {}}:

with pkgs;

mkShell {
  buildInputs = with pkgs; [
    black
    python310Packages.pyflakes
    python310Packages.isort
    nodejs
    nodePackages.pyright
  ];
}
