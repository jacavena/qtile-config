import os
import re
import socket
import subprocess
from libqtile import bar, layout, widget, hook
from libqtile.config import (
    Click,
    Drag,
    Group,
    KeyChord,
    Key,
    Match,
    ScratchPad,
    DropDown,
    Screen,
)
from libqtile.lazy import lazy
from widgets.volume import Volume

mod = "mod4"
terminal = "alacritty"

keys = [
    Key([mod], "x", lazy.spawn("timeout 1 random-wallpaper"), desc="Launch terminal"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "d", lazy.spawn("rofi -show drun"), desc="Launch terminal"),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "Escape", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "backslash", lazy.spawn("emacsclient -c -a emacs"), desc="Doom Emacs"),
    # Switch focus to specific monitor
    Key([mod], "w", lazy.to_screen(0), desc="Keyboard focus to monitor 1"),
    Key([mod], "e", lazy.to_screen(1), desc="Keyboard focus to monitor 2"),
    Key([mod], "period", lazy.next_screen(), desc="Move focus to next monitor"),
    Key([mod], "comma", lazy.prev_screen(1), desc="Move focus to prev monitor"),
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key(
        [mod],
        "h",
        lazy.layout.shrink_main(),
        lazy.layout.decrease_nmaster(),
        desc="Move focus to left",
    ),
    Key(
        [mod],
        "l",
        lazy.layout.grow_main(),
        lazy.layout.increase_nmaster(),
        desc="Move focus to right",
    ),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    # Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key(
        [mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"
    ),
    Key(
        [mod, "shift"],
        "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right",
    ),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key(
        [mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"
    ),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen"),
    Key([mod, "shift"], "f", lazy.window.toggle_floating(), desc="Toggle floating"),
    # Volume Control
    Key([mod], "minus", lazy.spawn("pamixer -d 5"), desc="Decrease Volume"),
    Key([mod], "equal", lazy.spawn("pamixer -i 5"), desc="Increase Volume"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    # Toggle between different layouts as defined below
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key(
        [],
        "Print",
        lazy.spawn('maimpick "area clipboard"'),
        desc="Print region",
    ),
    Key(
        [mod, "shift"],
        "equal",
        lazy.spawn("toggle-sink.sh"),
        desc="Change sink",
    ),
    Key(
        [mod],
        "F8",
        lazy.spawn("shutdown.sh"),
        desc="Shutdown",
    ),
    # Scratchpads
    Key(
        [mod, "control"],
        "a",
        lazy.group["scratchpad"].dropdown_toggle("Agenda"),
        desc="Org Agenda",
    ),
    Key(
        [mod, "control"],
        "m",
        lazy.group["scratchpad"].dropdown_toggle("Ferdium"),
        desc="Ferdium",
    ),
    Key(
        [mod, "control"],
        "c",
        lazy.spawn("org-capture -k i"),
        desc="Emacs Org Capture",
    ),
    KeyChord(
        [mod],
        "u",
        [
            Key(
                [],
                "u",
                lazy.group["scratchpad"].dropdown_toggle("Emacs Everywhere"),
                desc="Emacs Doom Everywhere",
            ),
            Key(
                [],
                "s",
                lazy.spawn(
                    "emacsclient -c -a 'emacs' -F '(quote (name . \"emacs-eshell\"))' --eval '(eshell)'"
                ),
                desc="Emacs Eshell",
            ),
        ],
    ),
]

groups = [
    Group("1", spawn="emacsclient -c -a 'emacs'"),
    Group("2", spawn="vivaldi"),
    Group("3"),
    Group("4"),
    Group("5"),
    Group("6"),
    Group("7"),
    Group("8"),
    Group("9"),
]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            # Key(
            #     [mod, "shift"],
            #     i.name,
            #     lazy.window.togroup(i.name, switch_group=True),
            #     desc="Switch to & move focused window to group {}".format(i.name),
            # ),
            # Or, use below if you prefer not to switch to that group.
            # mod1 + shift + letter of group = move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name),
                desc="move focused window to group {}".format(i.name),
            ),
        ]
    )

groups.append(
    ScratchPad(
        "scratchpad",
        [
            DropDown(
                "Emacs Everywhere",
                "doom +everywhere",
                x=0.01,
                y=0.01,
                width=0.50,
                height=0.50,
                opacity=1.0,
                on_focus_lost_hide=False,
                match=Match(wm_instance_class="emacs-everywhere"),
            ),
            DropDown(
                "Agenda",
                "emacsclient -c -a 'emacs' -F '(quote (name . \"emacs-agenda\"))' -e '(org-agenda-list)'",
                x=0.15,
                y=0.05,
                width=0.70,
                height=0.90,
                opacity=1.0,
                on_focus_lost_hide=False,
                match=Match(
                    wm_instance_class="emacs-agenda",
                ),
            ),
            DropDown(
                "Ferdium",
                "ferdium",
                x=0.15,
                y=0.05,
                width=0.70,
                height=0.90,
                opacity=1.0,
                on_focus_lost_hide=False,
                match=Match(
                    wm_class="Ferdium",
                ),
            ),
        ],
    )
)
# keys.extend(
#     [
#         Key(
#             [mod],
#             "m",
#             lazy.group["scratchpad"].dropdown_toggle("Ferdium"),
#             desc="Switch to group ~",
#         ),
#     ]
# )

layout_theme = {
    "border_width": 2,
    "margin": 8,
    "border_focus": "e1acff",
    "border_normal": "1d2330",
}

layouts = [
    layout.MonadTall(single_border_width=0, single_margin=0, **layout_theme),
    layout.Max(**layout_theme),
    # layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

widget_defaults = dict(
    font="Ubuntu Bold",
    fontsize=10,
    padding=2,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Pomodoro(length_long_break=20, length_short_break=6),
                widget.CurrentLayout(),
                widget.GroupBox(
                    font="Ubuntu Bold",
                    fontsize=11,
                    borderwidth=3,
                    active=["#dfdfdf", "#dfdfdf"],
                    inactive=["#c678dd", "#c678dd"],
                    highlight_color=["#1c1f24", "#1c1f24"],
                    highlight_method="line",
                ),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Systray(padding=5),
                widget.Net(
                    interface="wlo1",
                    format="Net: {down}↓↑{up}",
                    update_interval=3,
                    padding=5,
                ),
                widget.ThermalSensor(
                    threshold=70,
                    padding=5,
                    tag_sensor="Tccd1",
                ),
                widget.Memory(
                    mouse_callbacks={
                        "Button1": lambda: lazy.spawn(terminal + " -e btop")
                    },
                    fmt="Mem: {}",
                    measure_mem="G",
                    padding=5,
                ),
                Volume(
                    padding=5,
                    get_volume_command="pamixer --get-volume-human",
                    volume_up_command="pamixer --increase 5",
                    volume_down_command="pamixer --decrease 5",
                    mouse_callbacks={
                        "Button1": lazy.spawn("pavucontrol"),
                    },
                ),
                widget.Clock(format="%Y-%m-%d %a %I:%M %p"),
            ],
            24,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(role="pop-up"),
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
        Match(wm_class="Pavucontrol"),
        Match(wm_class="doom-capture"),
        Match(wm_class="PureRef"),
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None


@hook.subscribe.screens_reconfigured
def configure_screens():
    home = os.path.expanduser("~")
    subprocess.Popen(["timeout", "1", home + "/.local/bin/scripts/random-wallpaper"])


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
